
def addiere(a,b):
    """
    Addiert zwei Zahlen
    Parameter:
    - a (int): Die erste Zahl
    - b (int): Die zweite Zahl

    Rückgabewert:
    - int: Die Summe von "a" und "b"

    Beispiele:
    >>> addiere(1,2)
    5
    >>> addiere(2,2)
    4
    """
    return a+b
    

x=1
y=2
e = addiere(x,y)
print (e)