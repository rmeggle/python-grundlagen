# return:
def istGerade(x):
    if x % 2 == 0: 
        return True
    else:
        return False

for x in range(50):
    if istGerade(x):
        print (x, end=';')

exit()

# ##############################################################
# yield: 
def gibGeradeZahl(z=1):
    while True:
        z += 1
        if istGerade(z):
            yield z

gibGeradeZahlGenerator = gibGeradeZahl()
for i in range(50):
    print(gibGeradeZahlGenerator.send(None), end=';') # generator-object muss mit "none" gestartet werden


exit()
# ##############################################################
# yield in listen: 

meineListe = [x for x in range(1,10)]

def ListenGenerator(meineListe):
    for i in range(len(meineListe)):
        if istGerade(i):
            yield i

for ix in ListenGenerator(meineListe):
    print (ix, end=';')

