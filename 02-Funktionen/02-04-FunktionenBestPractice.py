"""
Die Prüfung if __name__ == "__main__": in Python ist eine gängige Praxis, 
um zu bestimmen, ob ein Python-Skript direkt ausgeführt wird oder in ein 
anderes Skript als Modul importiert wird. Der Ausdruck __name__ ist eine 
spezielle Variable, die automatisch vom Python-Interpreter gesetzt wird. 
Wenn ein Skript direkt ausgeführt wird, setzt Python __name__ auf den 
String "__main__". Wird das Skript jedoch als Modul in ein anderes Skript 
importiert, wird __name__ auf den Namen des Moduls gesetzt, unter dem es importiert wurde.

Wenn also das Script direkt ausgeführt wird, so wird die Funktion main aufgerufen. 
Wenn das Script später extern eingebunden wird, so steht die Funktion addiere 
dem Gesamtprojekt zur Verfügung.

=> Es können wesentlich leichter bei großeren und komplexen Projekten einzelne 
   Module getestet werden
"""

def addiere(a,b):
    return a+b

def main():
    print("Hauptprogramm")
    x=input("Gebe die erste Zahl ein:")
    y=input("Gebe die zweite Zahl ein:")
    e = addiere( int(x), int(y))
    print (e)


if __name__ == "__main__":
    main()