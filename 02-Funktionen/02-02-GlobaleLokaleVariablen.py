a = 42
b = 10 

def func1():
    print (a)
    # usw...

def func2():
    a = 12
    # usw...

def func3():
    t = a
    print (t)
    # usw...

#def func4():
#    t = a # geht nicht!!
#    a = 12

def func5():
    global a
    global b
    t = a  # geht nun!
    a = b
    b = t

print (a)
print (b)
func5()
print (a)
print (b)
