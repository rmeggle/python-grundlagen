def celsius_to_kelvin(celsius):
    """Rechnet Celsius in Kelvin um."""
    return celsius + 273.15

def kelvin_to_celsius(kelvin):
    """Rechnet Kelvin in Celsius um."""
    return kelvin - 273.15

def celsius_to_fahrenheit(celsius):
    """Rechnet Celsius in Fahrenheit um."""
    return (celsius * 9/5) + 32

def fahrenheit_to_celsius(fahrenheit):
    """Rechnet Fahrenheit in Celsius um."""
    return (fahrenheit - 32) * 5/9

def kelvin_to_fahrenheit(kelvin):
    """Rechnet Kelvin in Fahrenheit um."""
    return (kelvin - 273.15) * 9/5 + 32

def fahrenheit_to_kelvin(fahrenheit):
    """Rechnet Fahrenheit in Kelvin um."""
    return (fahrenheit - 32) * 5/9 + 273.15

def main():
    print("Umrechnungsoptionen:")
    print("1: Celsius nach Kelvin")
    print("2: Kelvin nach Celsius")
    print("3: Celsius nach Fahrenheit")
    print("4: Fahrenheit nach Celsius")
    print("5: Kelvin nach Fahrenheit")
    print("6: Fahrenheit nach Kelvin")
    
    auswahl = input("Bitte wählen Sie eine Option (1-6): ")
    
    if auswahl == "1":
        celsius = float(input("Bitte geben Sie die Temperatur in Grad Celsius ein: "))
        print(f"{celsius} Grad Celsius entsprechen {celsius_to_kelvin(celsius)} Kelvin.")
    elif auswahl == "2":
        kelvin = float(input("Bitte geben Sie die Temperatur in Kelvin ein: "))
        print(f"{kelvin} Kelvin entsprechen {kelvin_to_celsius(kelvin)} Grad Celsius.")
    elif auswahl == "3":
        celsius = float(input("Bitte geben Sie die Temperatur in Grad Celsius ein: "))
        print(f"{celsius} Grad Celsius entsprechen {celsius_to_fahrenheit(celsius)} Fahrenheit.")
    elif auswahl == "4":
        fahrenheit = float(input("Bitte geben Sie die Temperatur in Fahrenheit ein: "))
        print(f"{fahrenheit} Fahrenheit entsprechen {fahrenheit_to_celsius(fahrenheit)} Grad Celsius.")
    elif auswahl == "5":
        kelvin = float(input("Bitte geben Sie die Temperatur in Kelvin ein: "))
        print(f"{kelvin} Kelvin entsprechen {kelvin_to_fahrenheit(kelvin)} Fahrenheit.")
    elif auswahl == "6":
        fahrenheit = float(input("Bitte geben Sie die Temperatur in Fahrenheit ein: "))
        print(f"{fahrenheit} Fahrenheit entsprechen {fahrenheit_to_kelvin(fahrenheit)} Kelvin.")
    else:
        print("Ungültige Auswahl. Bitte wählen Sie eine Option zwischen 1 und 6.")

if __name__ == "__main__":
    main()
