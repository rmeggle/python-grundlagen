import csv
import os

def get_ortsnamen(file_name):
    liste_orte=set() 
    with open(file_name, 'r') as csv_datei:
        # Verwenden der ersten Zeile als Schlüsselnamen
        csv_reader = csv.DictReader(csv_datei)
        for row in csv_reader:
            ort = row['ort']  # Liest den Wert der Spalte "ort"
            liste_orte.add(ort)
    return liste_orte

def find_pallindrom(liste_orte):
    pallindrom_ort = set()
    for ort in liste_orte:
        if ort.lower() == ort[::-1].lower():
            pallindrom_ort.add(ort)
    return pallindrom_ort


def main():
    print("Hauptprogramm")
    file_name = os.path.join(os.getcwd(), "tmp", "plz_einwohner.csv")
    ortsnamen_de = get_ortsnamen(file_name)
    ortsnamen_pallindrom = find_pallindrom(ortsnamen_de)

    if ortsnamen_pallindrom:
        print("Folgende Ortsnamen sind in Deutschland ein Pallindrom:")
        for ort in ortsnamen_pallindrom:
            print (ort)
    else:
        print("Es wurden keine Ortsnamen gefunden, die ein Pallindrom darstellen")
    


if __name__ == "__main__":
    main()