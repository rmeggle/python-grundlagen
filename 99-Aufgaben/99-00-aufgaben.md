# Aufgaben

## 01 Hangman
Das Spiel "Hangman" (Galgenmännchen) ist ein einfaches Worträtselspiel. Der Rechner denkt sich ein Wort aus, und der Anwender versucht, es Buchstabe für Buchstabe zu erraten. Für jeden falschen Versuch wird ein Teil des Galgenmännchens gezeichnet. Das Ziel ist es, das Wort zu erraten, bevor das Galgenmännchen vollständig gezeichnet ist.
Spielablauf:

* Ein Wort wird ausgewählt und für jeden Buchstaben des Wortes wird ein Unterstrich (_) aufgeschrieben, um die versteckten Buchstaben darzustellen.
* Der ratende Spieler nennt einen Buchstaben.
* Wenn der Buchstabe im Wort vorkommt, werden alle Vorkommen dieses Buchstabens im Wort aufgedeckt.
* Wenn der Buchstabe nicht im Wort vorkommt, wird ein Teil des Galgenmännchens gezeichnet.
* Das Spiel endet, wenn:
  * Der Spieler das Wort vollständig errät (Gewinn).
  * Das Galgenmännchen vollständig gezeichnet ist (Verlust).

Tipps:
Wenn die ausgedachten Wörter in einer Liste sind: 
```python
woerter = ['python', 'java', 'kotlin', 'javascript']
```
So kann ein Wort zufällt mit "random" und "choice" herausgepickt werden:
```python
wort = random.choice(woerter)
```



## 02 Temperaturen. 

Erstelle eine Anwendung, die es einem Benutzer ermöglicht, zwischen verschiedenen Temperatureinheiten (Celsius, Kelvin, Fahrenheit) zu wählen und die Umrechnung durchzuführen.

Dies kann Dir sicher bei der Erstellung weiterhelfen: 

| Beschreibung            | Formel                                  |
| ----------------------- | --------------------------------------- |
| Fahrenheit nach Celsius | Celsius=(Fahrenheit - 32) * 5/9         |
| Fahrenheit nach Kelvin  | Kelvin=(Fahrenheit - 32) * 5/9 + 273.15 |
| Celsius nach Fahrenheit | Fahrenheit=(Celsius * 9/5) + 32         |
| Celsius nach Kelvin     | Kelvin= Celsius  + 273.15               |
| Kelvin nach Fahrenheit  | Fahrenheit=(Kelvin - 273.15) * 9/5 + 32 |
| Kelvin nach Celsius     | Celsius= Kelvin - 273.15                |


Zusatzaufgabe: 
**Die tiefste mögliche Temperatur ist den absoluten Nullpunkt: 0 Kelvin.**


## 03 Berechnung des BMI 

Die Anwendung soll die einzelnen Variablen abfragen und die Gewichtsklasse als Ergebnis ausgeben. 
Die Formel hierzu ist: 
$$
BMI = Gewicht (kg) / Grösse (m)^2
$$

BMI-Tabelle: 

| BMI         | Gewichtsklasse       |  
| ----------- | -------------------- |  
| Unter 18.5  | Untergewicht         |  
| 18.5 - 24.9 | Normalgewicht        |  
| 25.0 - 29.9 | Vorstufe Übergewicht |  
| 30.0 - 34.9 | Übergewicht Grad 1   |  
| 35.0 -39.9  | Übergewicht Grad 2   |  
| über 40     | Übergewicht Grad 3   |  

Optimaler BMI nach Alter: 

| Alter | optimaler BMI |  
| ----- | ------------- |  
| 19-24 | 19-24         |  
| 25-34 | 20-25         |  
| 35-44 | 21-26         |  
| 45-54 | 22-27         |  
| 55-64 | 23-28         |  

( Quelle: World Health Organization Europe )

## 03 Ortsnamen

### Ortsnamen
( Ein Palindrom ist ein Wort, welches das selbe Wort ergibt, wenn man es rückwärts liest, Beispiel: Otto, Rentner...)
Welche Stadt in Deutschland ist ein Palindrom?
Lade hierzu eine passende Ortsliste aus dem Internet: https://www.suche-postleitzahl.org/downloads
Alternativ verwende die Liste aus dem lokalen Repository. 

## etwas aus dem Internet zum laufen bekommen

Bekommst Du das folgende Beispiel zum laufen?

~~~
import matplotlib.pyplot as plt
import pandas as pd

#Daten
link = "https://data.giss.nasa.gov/gistemp/graphs_v4/graph_data/Global_Mean_Estimates_based_on_Land_and_Ocean_Data/graph.csv"
Temp_NASA = pd.read_csv(link, header=1) # einlesen

#Plot
plt.plot(Temp_NASA["Year"],Temp_NASA["No_Smoothing"]);
plt.ylabel("Jahresmitteltemperaturabweichung \n gegenüber 1951-1980 [°C]");
plt.xlabel("Jahr");
~~~

Quelle: https://fkaule.github.io/Software_wissenschaftliches_Arbeiten/Python/Python_Uebersicht.html

Frage: Was ist denn "pandas"?




