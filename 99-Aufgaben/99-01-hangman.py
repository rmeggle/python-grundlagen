import random

# Liste von Wörtern
woerter = ['python', 'java', 'kotlin', 'javascript']

# Zufälliges Wort auswählen
wort = random.choice(woerter)
geratene_buchstaben = set()
falsche_versuche = 0
max_fehler = 7

# Das Rätsel
raetsel = ['_'] * len(wort)

def zeige_raetsel():
    print(' '.join(raetsel))

while falsche_versuche < max_fehler and '_' in raetsel:
    zeige_raetsel()
    buchstabe = input("Rate einen Buchstaben: ").lower()

    if buchstabe in geratene_buchstaben:
        print("Diesen Buchstaben hast du schon geraten.")
        continue

    geratene_buchstaben.add(buchstabe)

    if buchstabe in wort:
        for index, w_buchstabe in enumerate(wort):
            if w_buchstabe == buchstabe:
                raetsel[index] = buchstabe
    else:
        falsche_versuche += 1
        print(f"Falsch! Du hast noch {max_fehler - falsche_versuche} Versuche.")

if '_' not in raetsel:
    print("Glückwunsch! Du hast das Wort erraten:", ''.join(raetsel))
else:
    print("Leider verloren! Das Wort war:", wort)
