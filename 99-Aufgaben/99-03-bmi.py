

gewicht_kg = input("Bitte das Gewicht in kg angeben:")
größe_m = input("Bitte die Körpergröße in m angeben:")
alter_jahren = input("Wie alt bist Du?")

bmi_tabelle = [ 
                { "min_value" : 0, "max_value" : 18.5, "Gewichtsklasse": "Untergewicht" }, 
                { "min_value" : 18.5, "max_value" : 24.9, "Gewichtsklasse" : "Normalgewicht" }, 
                { "min_value" : 24.9, "max_value" : 29.9, "Gewichtsklasse" : "Vorstufe Übergewicht" }, 
                { "min_value" : 29.9, "max_value" : 34.9, "Gewichtsklasse" : "Übergewicht Grad 1" }, 
                { "min_value" : 34.9, "max_value" : 39.9, "Gewichtsklasse" : "Übergewicht Grad 2" }, 
                { "min_value" : 39.9, "max_value" : 100, "Gewichtsklasse" : "Übergewicht Grad 3" } 
            ]
            
alter_tabelle = [   { "min_age": 19, "max_age": 24, "opt_bmi_min": 19, "opt_bmi_max": 24 }, 
                    { "min_age": 24, "max_age": 34, "opt_bmi_min": 20, "opt_bmi_max": 25 }, 
                    { "min_age": 34, "max_age": 44, "opt_bmi_min": 21, "opt_bmi_max": 26 }, 
                    { "min_age": 44, "max_age": 54, "opt_bmi_min": 22, "opt_bmi_max": 27 }, 
                    { "min_age": 54, "max_age": 64, "opt_bmi_min": 23, "opt_bmi_max": 28 }
            ]
def ermittle_gewichtsklasse(bmi_index,min_value, max_value,gewichtsklasse):
    if bmi_index > min_value and bmi_index < max_value:
        return gewichtsklasse
    else:
        return "-"

def optimaler_bmi(alter):
    min_bmi = 0
    max_bmi = 0

    for key in alter_tabelle:
        if int(alter) > int(key["min_age"]) and int(alter) < int(key["max_age"]) :
            min_bmi = key["opt_bmi_min"]        
            max_bmi = key["opt_bmi_max"]        

    return [min_bmi, max_bmi]

try:
    bmi_index = float(gewicht_kg) / float(größe_m) ** 2

    mein_optimaler_bmi = optimaler_bmi(alter_jahren)
    
    if bmi_index > mein_optimaler_bmi[0] and bmi_index < mein_optimaler_bmi[1]:
        print("BMI Index im normalen bereich")
    else:
        print("Lieber mal heute Abend auf Kässpatzn und dem Weissbier verzichten")
        
    
    for k in bmi_tabelle:
        min_value = k["min_value"]
        max_value = k["max_value"]
        gewichtsklasse = k["Gewichtsklasse"]
        print(ermittle_gewichtsklasse(bmi_index,min_value,max_value,gewichtsklasse))
except:
    print ("da ist was schief gelaufen!")