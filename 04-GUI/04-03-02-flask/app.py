from flask import Flask, render_template, url_for, request

"""
Flask ist ein schlankes Webframework zum Programmieren von Webanwendungen mit Python. 
Es wurde von dem österreichischen Open-Source-Entwickler Armin Ronacher entworfen. 
Das Framework ist als Bibliothek für Python installierbar und verfolgt einen minimalistischen 
Ansatz. Es benötigt das WSGI-Toolkit „Werkzeug“ und die Template-Engine „Jinja“. 
"""

# web source: https://www.youtube.com/watch?v=Z1RJmh_OqeA
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/', methods=['POST','GET'])
def show_content():
    if request.method == 'POST':
        feedback = ["Post button is been pressed now","value 2","Value 4"]
        return render_template('index.html', feedback=feedback)



if __name__ == "__main__":
    app.run(debug=True)