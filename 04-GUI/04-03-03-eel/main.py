
import eel
'''
Einfach ausgedrückt: Die eel-Bibliothek wird verwendet, um HTML-Benutzeroberflächen für 
Python-Anwendungen für den Desktop (nicht für das Web) zu erstellen. 
Es gibt zwar Tools wie Tkinter, bei denen die GUI in Python selbst codiert werden kann, 
aber die Einschränkungen sind ziemlich hoch und es gibt nicht so viel Freiheit 
wie bei der Verwendung von HTML und JavaScript.

Hier kommt eel ins Spiel. Es ermöglicht uns Anwendungen zu erstellen, bei denen das 
Frontend in HTML und CSS codiert ist, während die eigentliche Programmlogik dahinter in Python läuft.

Um das Beispiel auszuführen, werden folgende beide Bibliotheken benötigt:
pip install eel
pip install eel[jinja2]
'''

@eel.expose
def say_hello():
    print("I will give now you an simple feedback")


eel.init("view")
eel.start("index.html", block=False)

while True: 
    print ("I am running")
    eel.sleep(1)