
import eel

eel.init('view')                     # Give folder containing web files

@eel.expose                         # Expose this function to Javascript
def handle_input(x):
    print('%s' % x)

eel.say_hello_js('connected!')   # Call a Javascript function

eel.start('index_input_example.html', size=(500, 300))    # Start