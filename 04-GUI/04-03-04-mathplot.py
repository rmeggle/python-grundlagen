import matplotlib.pyplot as plt
days = list(range(1,9))
celsius_values = [25.6, 24.1, 26.7, 28.3, 27.5, 30.5, 32.8, 33.1]
plt.plot(days, celsius_values, color="red", linewidth=0.5, linestyle="-.")
plt.xlabel('Messpunkt')
plt.ylabel('Grad Celsius')
plt.grid()
plt.show()

