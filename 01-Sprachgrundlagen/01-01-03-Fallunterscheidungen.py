# Vergleichsoperatoren:
#
# ==  Gleich 
# !=  Ungleich
# >   Größer als
# <   Kleiner als
# >=  Größer als oder gleich
# <=  Kleiner als oder gleich
# 

x = 1
y = 2

print (x == y)  # False
print (x >  y ) # False
print (x <  y ) # True
print (x >= y ) # False
print (x <= y ) # True
print (x != y ) # True

# Logisches UND:
# True <-> True and True
# False <-> True and False
# False <-> False and False
# False <-> False and True

print ( 1 < 2 and 3 < 4 ) # True
print ( 1 > 2 and 3 < 4 ) # False
print ( 1 < 2 and 3 > 4 ) # False
print ( 1 > 2 and 3 > 4 ) # False

# Logisches ODER:
# True <-> True or True
# True <-> True or False
# True <-> False or True
# False <-> False or False

print ( 1 < 2 or 3 < 4 ) # True
print ( 1 > 2 or 3 < 4 ) # True
print ( 1 < 2 or 3 > 4 ) # True
print ( 1 > 2 or 3 > 4 ) # False

# Nicht zu verwechseln mit Wertzuweisungen:
alter = 16
erreichte_punktzahl = 85
hat_fuehrerschein = True
ist_betrunken=False

#
# if-else
#
if alter >= 18:
    print("Du bist volljährig.")
else:
    print("Du bist minderjährig.")
#
# if-elif-else-Anweisung
#
if erreichte_punktzahl >= 90:
    print("Sehr gut")
elif erreichte_punktzahl >= 80:
    print("Gut")
elif erreichte_punktzahl >= 70:
    print("Befriedigend")
elif erreichte_punktzahl >= 60:
    print("Ausreichend")
else:
    print("Nicht bestanden")

#
# Verschachtelte if-Anweisung
#
if alter >= 18:
    if hat_fuehrerschein:
        print("Du darfst Auto fahren.")
        if ist_betrunken:
            print("...aber Du darst nicht fahren, da Du betrunken bist!")
    else:
        print("Du bist zwar volljährig, hast aber keinen Führerschein.")
else:
    print("Du bist minderjährig und darfst gar nicht Auto fahren.") 

#
# if-Anweisungen mit logischen Operatoren
#
if alter >= 18 and hat_fuehrerschein and not ist_betrunken:
    print("Du darfst Auto fahren.")
else:
    print("Du darfst nicht Auto fahren.")
#
# "tenärerer"-Ausdruck
#
status = "volljährig" if alter >= 18 else "minderjährig"
print(f"Du bist {status}.")
