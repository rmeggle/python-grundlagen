def division():
    try:
        num1 = input("Gib die erste Zahl ein: ")
        num2 = input("Gib die zweite Zahl ein: ")
        ergebnis = int(num1) / int(num2)
    except ZeroDivisionError:
        print("Fehler: Division durch Null ist nicht möglich.")
    except ValueError:
        print("Fehler: Bitte gib nur numerische Werte ein.")
    except Exception as e:
        print(f"Ein unerwarteter Fehler ist aufgetreten: {e}")
    else:
        print(f"Das Ergebnis der Division von {num1} durch {num2} ist {ergebnis}")
    finally:
        print("Versuch der Division abgeschlossen.")

# Funktion aufrufen
division()
