# Kommentare können einzeilig sein

"""
Kommentare können aber auch mehrzeilig
sein. 
"""


#
# Ausgabe von Werten mit "print"
#

# Python akzeptiert "single", "double" und "tripple" 
print ("hello world")
print ('hallo world')
print (""" hello world """)

# Auch die Ausgabe mehrerer Zeichenketten ist möglich:
print("Vorname","Nachname","E-Mail","PLZ","Ort")

print ("Vorname", end='.')

# Mit "sep" kann ein Trennzeihen, wie z.B. ein ";" zwischen die Werte eingesetzt werden:
print("Vorname","Nachname","E-Mail","PLZ","Ort",sep=";")

name_first = "Donald"
name_last = "Duck"
monthly_salary = 123456.7890
print (name_first,name_last) # Donald Duck
#
# Methoden zur String-Interpolation
#
# 0. Zeichenkettenoperator
print ("Hello mr. " + name_first + " " + name_last + ", you've earned " + str(monthly_salary) + " EUR!")
# 1. Mittels dem modulo-Operator
print ("Hello mr. %s %s, you've earned %.2f EUR!" % (name_first , name_last, monthly_salary) )
# 2. Die str.format - Methode
print ("Hello mr. {} {}, you've earned {} EUR!".format(name_first,name_last,monthly_salary))
print ("Hello mr. {0} {1}, you've earned {2} EUR!".format(name_first,name_last,monthly_salary))
print ("Hello mr. {firstname} {lastname}, you've earned {salary} EUR!".format(firstname=name_first, lastname=name_last, salary=monthly_salary))
# (Siehe dazu Kapitel 01-04-03-Set.py)
my_dict = {
            "firstname": name_first,
            "lastname":name_last,
            "salary":monthly_salary
        }
print ("Hello mr. {firstname} {lastname}, you've earned {salary} EUR!".format(**my_dict))
# 3. f-strings
print (f"Hello mr. {name_first} {name_last}, you've earned {monthly_salary} EUR!")
print (f"Hello mr. {name_first} {name_last}, you've earned {monthly_salary:.2f} EUR!")
print (f"Hello mr. {name_first} {name_last}, you've earned {monthly_salary:,.2f} EUR!")
