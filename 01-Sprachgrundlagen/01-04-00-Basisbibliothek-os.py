#
# os ist eine Grundlegende Basisbibliothek zur Interaktion mit dem Betriebsystem
#

import os


# Pfadteile mit join zusammensetzen:
example_file = os.path.join(os.getcwd(), "../", "tmp", "plz_einwohner.csv")

#
# os-Funktionen im Detail:
#

# Aktuelles Arbeitsverzeichnis abfragen
current_directory = os.getcwd()
print(f"Aktuelles Verzeichnis: {current_directory}")
# Pfad des aktuellen Skripts
script_path = os.path.realpath(__file__)
print(f"Pfad des aktuellen Skripts: {script_path}")

# Verzeichnis des aktuellen Skripts
script_dir = os.path.dirname(script_path)
print(f"Verzeichnis des aktuellen Skripts: {script_dir}")

# Liste aller Dateien und Verzeichnisse im aktuellen Verzeichnis
entries = os.listdir(current_directory)
print(f"Einträge im aktuellen Verzeichnis: {entries}")

# Wert einer Umgebungsvariablen abfragen
home_dir = os.getenv("HOME")  # Für Unix/Linux/Mac
print(f"Home-Verzeichnis: {home_dir}")

print(f"Vollständiger Pfad zur Beispieldatei: {example_file}")

# Wie heisst Die Datei in einem Pfad?
basename = os.path.basename(example_file)
print(f"Name der Beispieldatei: {basename}")

# Verzeichnisname eines Pfades abfragen
dirname = os.path.dirname(example_file)
print(f"Verzeichnisname der Beispieldatei: {dirname}")

# Prüfen, ob eine Datei existiert
exists = os.path.exists(example_file)
print(f"Existiert der Pfad? {exists}")

# Prüfen, ob ein Pfad eine Datei ist
isfile = os.path.isfile(example_file)
print(f"Ist der Pfad eine Datei? {isfile}")

# Prüfen, ob ein Pfad ein Verzeichnis ist
isdir = os.path.isdir(example_file)
print(f"Ist der Name etwa ein Verzeichnis? {isdir}")