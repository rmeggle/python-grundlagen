#
# math ist eine Grundlegende Basisbibliothek für für Daten, Zeiten und Zeitintervalle, formatieren und manipulieren des Datums.
#

import datetime

# 1. Aktuelles Datum und Uhrzeit
# Aktuelles Datum und Uhrzeit
jetzt = datetime.datetime.now()
print(f"Aktuelles Datum und Uhrzeit: {jetzt}")

# 2. Spezifisches Datum und Uhrzeit erstellen

# Spezifisches Datum und Uhrzeit erstellen
dt = datetime.datetime(2024, 1, 1, 15, 30)
print(f"Spezifisches Datum und Uhrzeit: {dt}")

# 3. Datum und Uhrzeit formatieren

jetzt = datetime.datetime.now()

# Datum und Uhrzeit in einem spezifischen Format ausgeben
formatiert = jetzt.strftime("%Y-%m-%d %H:%M:%S")
print(f"Formatiertes Datum und Uhrzeit: {formatiert}")

# 4. String in `datetime` umwandeln

datum_zeit_str = '01/01/2024 15:30'

# String in datetime umwandeln
datum_zeit_obj = datetime.datetime.strptime(datum_zeit_str, '%d/%m/%Y %H:%M')

print(f"Umgewandeltes Datum und Uhrzeit: {datum_zeit_obj}")

# 5. Differenz zwischen zwei Datums-/Zeitangaben

# Zwei Datums-/Zeitangaben
dt_start = datetime.datetime(2024, 1, 1)
dt_ende = datetime.datetime(2024, 2, 1)

# Differenz berechnen
differenz = dt_ende - dt_start

print(f"Tage zwischen den Daten: {differenz.days}")


# 6. Hinzufügen oder Subtrahieren von Tagen

# Aktuelles Datum
jetzt = datetime.datetime.now()

# 10 Tage hinzufügen
in_zehn_tagen = jetzt + datetime.timedelta(days=10)

# 10 Tage subtrahieren
vor_zehn_tagen = jetzt - datetime.timedelta(days=10)

print(f"In 10 Tagen: {in_zehn_tagen}")
print(f"Vor 10 Tagen: {vor_zehn_tagen}")

# 7. Wochentag ermitteln

# Ein spezifisches Datum
dt = datetime.datetime(2024, 1, 1)

# Wochentag als Zahl (Montag ist 0 und Sonntag ist 6)
wochentag_nummer = dt.weekday()

# Wochentag als Namen
wochentage = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag']
wochentag_name = wochentage[wochentag_nummer]

print(f"Der 1. Januar 2024 ist ein {wochentag_name}.")
