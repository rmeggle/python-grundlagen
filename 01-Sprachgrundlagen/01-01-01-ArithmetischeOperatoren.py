num_1 = 42
num_2 = 7
num_3 = 6.1
print(num_1 + num_2) # Addition
print(num_1 - num_2) # Subtraktion
print(num_1 * num_2) # Multiplikation
print(num_1 / num_2) # Division
print(num_1 ** num_2) # Exponent
print(num_1 % num_2) # Rest einer ganzzahligen Division (Modulo)
print(num_1 // num_3) # Ganzzahliger Anteil der Integer Division