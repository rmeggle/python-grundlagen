# Unterschied Listen -> Tupel: Tupel haben Struktur, Listen haben Reihenfolge.

zahlenTupel = (2, 3, 5, 7, 11)
print (zahlenTupel)

print(zahlenTupel[0])
print(zahlenTupel[1])
print(zahlenTupel[2])
# ...
print(zahlenTupel[-1])
print(zahlenTupel[-2])
print(zahlenTupel[-3])

# Tupel sind unveränderlich und enthalten eine heterogene Sequenz!
# zahlenTupel[0]=4711 # <- Fehler!

