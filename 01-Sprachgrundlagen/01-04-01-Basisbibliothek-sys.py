#
# sys ist eine Grundlegende Basisbibliothek für das Python-Laufzeitumfeld
#

import sys

#
# Beispiele für die Basisbibliothek sys
#

# Ausgabe der Python-Version
print("Python-Version:", sys.version)

#
# Hinzufügen eines neuen Pfades zur sys.path-Liste - Nützlich bei größeren Projekte
#
new_path = '/pfad/zu/meinem/modul'
if new_path not in sys.path:
    sys.path.append(new_path)

# Überprüfen, ob eine Bedingung erfüllt ist, und das Script mit einem Fehlercode beenden
some_condition = False
if not some_condition:
    sys.exit(1)  # Beendet das Skript mit Exit-Code 1

sys.exit(0)  # Beendet das Skript erfolgreich mit Exit-Code 0