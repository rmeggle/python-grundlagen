zahlenListe = [2, 3, 5, 7, 11]

print(zahlenListe)

print(zahlenListe[0])
print(zahlenListe[1])
print(zahlenListe[2])
# ...
print(zahlenListe[-1])
print(zahlenListe[-2])
print(zahlenListe[-3])
# ...
#print(zahlen[-6]) # -> FEHLER!

# Anzahl der Elemente in der Liste ermitteln:
print (len(zahlenListe))

personenListe = ["Donald","Duck", 45, True, None]
print (personenListe)

liste1D = [1,2]
print (liste1D)

liste2D = [[1,2],[3,4]]
print (liste2D)
print (liste2D[0]) # -> [1,2]
print (liste2D[0][0]) # -> 1
print (liste2D[0][1]) # -> 2

liste3D = [[[1,2,3],[3,4,5]]]
print (liste3D)
print (liste3D[0]) # -> [[1, 2, 3], [3, 4, 5]]
print (liste3D[0][0]) # -> [1, 2, 3]
print (liste3D[0][0][0]) # -> 1

