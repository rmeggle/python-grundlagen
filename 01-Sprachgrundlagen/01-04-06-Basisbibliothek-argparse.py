#
# Siehe Kapitel "Funktionen"
#

import argparse

# Erstellen des Parser-Objekts
parser = argparse.ArgumentParser(description='Ein einfaches Beispiel.')

# Argument hinzufügen
parser.add_argument('echo', help='Gibt den eingegebenen Text zurück.')
# Optionales Argument hinzufügen
parser.add_argument('--verbose', '-v', action='store_true', help='Abzeigen von Detailinformationen.')
# Argument mit Wert hinzufügen
parser.add_argument('--quadrat', type=int, help='Gebe eine Ganzzahl ein')
# Argument, das mehrere Werte akzeptiert
# z.B. python script.py --multi 1 2 3
parser.add_argument('--multi', nargs='+', type=int, help='Akzeptiert mehrere Werte.')
# Argument mit Standardwert und spezifischem Datentyp
parser.add_argument('--zahl', type=int, default=10, help='Eine Zahl (Standard: 10).')

args = parser.parse_args()

