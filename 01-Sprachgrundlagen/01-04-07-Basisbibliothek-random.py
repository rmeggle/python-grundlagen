#
# Basisbibliothek "random" zum Generieren von Zufallswerten
#

import random

# Erzeugen einer Zufallszahl zwischen 0 und 1
zahl = random.random()
print(f"Zufallszahl zwischen 0 und 1: {zahl}")

# Erzeugen einer ganzen Zufallszahl zwischen a und b, einschließlich beider Enden
zahl = random.randint(1, 10)
print(f"Ganze Zufallszahl zwischen 1 und 10: {zahl}")

# Erzeugen einer Liste von 5 Zufallszahlen zwischen 1 und 100
zahlen = [random.randint(1, 100) for _ in range(5)]
print(f"Liste von Zufallszahlen: {zahlen}")

# Zufälliges Element auswählen
elemente = ['Apfel', 'Banane', 'Kirsche', 'Dattel']

element = random.choice(elemente)
print(f"Zufällig ausgewähltes Element: {element}")

# Liste mischen
elemente = [1, 2, 3, 4, 5]

random.shuffle(elemente)
print(f"Gemischte Liste: {elemente}")

#
# Weiterführende Funktionen für Statistik (Zufallszahlen mit spezifischer Verteilung oder Auswahl zufälliger Elemente mit Gewichtung)
# 
mu = 0  # Mittelwert
sigma = 1  # Standardabweichung
zahl = random.gauss(mu, sigma)
print(f"Normalverteilte Zufallszahl (mu=0, sigma=1): {zahl}")

elemente = ['Apfel', 'Banane', 'Kirsche']
gewichte = [10, 1, 1]  # Höhere Wahrscheinlichkeit, 'Apfel' zu wählen

# Zufälliges Element mit Gewichtung auswählen
element = random.choices(elemente, weights=gewichte, k=1)[0]
print(f"Zufällig ausgewähltes Element mit Gewichtung: {element}")
