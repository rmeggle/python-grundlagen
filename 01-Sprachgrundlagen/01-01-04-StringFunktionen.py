#
# Übersicht über die String-Funktionen
#

#
# 1. Manipulation
#
print("hello world".capitalize())  # Hello world
print("hello world".upper())  # HELLO WORLD
print("HELLO WORLD".lower())  # hello world
print("hello world".title())  # Hello World
print("hello WORLD".swapcase())  # HELLO world
#
# 2. Suchen und Erstetzen
#
print("hello world".find("world"))  # 6
print("hello world".index("world"))  # 6
print("hello world".replace("world", "Python"))  # hello Python
#
# 3. Prüfen/Untersuchen
#
print("hello world".startswith("hello"))  # True
print("hello world".endswith("world"))  # True
print("helloworld".isalpha())  # True (Beachte: Leerzeichen entfernt)
print("12345".isdigit())  # True (Beispiel geändert, da "hello world" keine Ziffern enthält)
print("hello123world".isalnum())  # True (Beachte: Leerzeichen und "world" entfernt)
print(" ".isspace())  # True (Beispiel geändert, da "hello world" nicht nur Leerzeichen enthält)
#
# 4. Teilung und Verbindung
#
print("hello world".split())  # ['hello', 'world']
print("hello world world".rsplit(" ", 1))  # ['hello world', 'world']
print("hello\nworld".splitlines())  # ['hello', 'world']
print(", ".join(["hello", "world"]))  # hello, world
#
# 5. Trimmen und Anpassen
#
print("  hello world  ".strip())  # hello world
print("  hello world".lstrip())  # hello world
print("hello world  ".rstrip())  # hello world
# 
# 6. der "slice"-Syntax [start:stop:step]
#
text_beispiel="Hallo Welt"
print(text_beispiel)     # Hallo Welt
print(text_beispiel[:])     # Hallo Welt
print(text_beispiel[0])     # H
print(text_beispiel[1])     # a
print(text_beispiel[:5])   # Hallo
print(text_beispiel[6:])    # Welt
print(text_beispiel[::2])   # HloWl
print(text_beispiel[::-1])  # tleW ollaH
print(text_beispiel[9:4:-1]) # tleW 
print(text_beispiel[1:7:2])  # al 
# Negative Indizes
print(text_beispiel[-1])    # t (letztes Zeichen)
print(text_beispiel[-4:-1]) # Wel (von -4 bis -1)
print(text_beispiel[:-3])   # Hallo W (alles bis auf die letzten drei Zeichen)
print(text_beispiel[-3:])   # elt (die letzten drei Zeichen)




