#
# Protokollieren von Nachrichten in Anwendungen
#

import logging

### 1. Einfaches Logging



# Konfigurieren des Loggings (Standardmäßig werden Warnungen und kritischere Nachrichten geloggt)
logging.basicConfig(level=logging.INFO)

# Log-Nachrichten verschiedener Schweregrade
logging.debug('Dies ist eine Debug-Nachricht.')
logging.info('Dies ist eine Info-Nachricht.')
logging.warning('Dies ist eine Warnung.')
logging.error('Dies ist ein Fehler.')
logging.critical('Dies ist eine kritische Nachricht.')

# 2. Logging in eine Datei


# Konfigurieren des Loggings, um Nachrichten in eine Datei zu schreiben
logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

# Log-Nachricht
logging.warning('Dies ist eine Warnung, die in eine Datei geschrieben wird.')

# 3. Formatierung von Log-Nachrichten

# Konfigurieren des Loggings mit einer spezifischen Nachrichtenformatierung
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logging.info('Dies ist eine Info-Nachricht mit Zeitstempel.')

# 4. Logging auf verschiedenen Levels

# Konfigurieren des Loggings auf dem DEBUG-Level
logging.basicConfig(level=logging.DEBUG)

logging.debug('Dies ist eine Debug-Nachricht.')
logging.info('Dies ist eine Info-Nachricht.')

# 5. Erstellen eines eigenen Loggers

# Erstellen eines eigenen Loggers
logger = logging.getLogger('meinLogger')
logger.setLevel(logging.DEBUG)

# Erstellen eines Handlers, der Log-Nachrichten in eine Datei schreibt
handler = logging.FileHandler('meinLogger.log')
handler.setLevel(logging.DEBUG)

# Erstellen eines Formatters und Hinzufügen des Formatters zum Handler
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# Hinzufügen des Handlers zum Logger
logger.addHandler(handler)

# Log-Nachrichten
logger.debug('Dies ist eine Debug-Nachricht.')
logger.info('Dies ist eine Info-Nachricht.')


# 6. Verwendung verschiedener Handler

# Erstellen eines Loggers
logger = logging.getLogger('meinLogger')
logger.setLevel(logging.DEBUG)

# Konsole Handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.ERROR)

# Datei Handler
file_handler = logging.FileHandler('meinLogger.log')
file_handler.setLevel(logging.DEBUG)

# Formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

# Handler zum Logger hinzufügen
logger.addHandler(console_handler)
logger.addHandler(file_handler)

# Log-Nachrichten
logger.debug('Debug-Nachricht.')
logger.error('Fehler-Nachricht.')
