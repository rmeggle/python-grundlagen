"""
Fehlende Module können mit pip nachinstalliert werden: 
pip install <PAKETNAME>

Doch werden diese Pakete dann anschließend auf dem System installiert und man hat keine Kontrolle mehr, welche
nun aktuell installiert werden müssen, sollte das Projekt auf einem anderen Rechner laufen. 
Lösung: venv

venv legt eine virtuelle Umgebung an, in der alle Pakete die mittels pip installiert werden, eingerichtet werden. 
AKtiviert wird venv mit:

python -m venv .venv

Aktiviert wird es mittels: 
source .venv/bin/activate

Deaktivert wird es mittels:
deactivate


requirements.txt

Die requirements.txt-Datei ist ein Standardformat in der Python-Entwicklung, das verwendet wird, 
um eine Liste von Paketabhängigkeiten für ein Projekt zu definieren. 

Gernerieren einer requirements.txt:

pip freeze > requirements.txt

Installieren aller notwendigen Abhängigkeiten mittels einer requirements.txt:

pip install -r requirements.txt

Tipp: Halten Sie Ihre requirements.txt-Datei aktuell, um von Sicherheitsupdates und neuen Funktionen der Pakete zu profitieren.

"""