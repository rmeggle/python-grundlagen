# Es gibt folgende Datentypen: 
# a) für ganze Zahlen: int (oder long)
# für Gleitkommazahlen: float
# für komplexe Zahlen: compex
# für boolsche Werte: bool

a=3
print(type(a))

b = "text"
print(type(b))

c = 1.0
print(type(c))

d = True
print(type(d))

e = 1 + 3j
print(type(e))

# Umwandlung zwischen numerischen Datentypen: 
x=3  # x ist int
x = float(x)
print(type(x)) # x ist nun "float"

# ... aber nicht unproblematisch: 
# int(5.0000001) # => 5
# int(5.9999999) # => 5

