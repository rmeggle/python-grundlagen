#
# math ist eine Grundlegende Basisbibliothek für mathematische Berechnungen, die über die Grundrechenarten hinausgehen
#

import math

#
# Runden einer Zahl
#
zahl = 4.35
gerundet = math.ceil(zahl)  # Aufrunden
print(f"{zahl} aufgerundet ist {gerundet}")

gerundet = math.floor(zahl)  # Abrunden
print(f"{zahl} abgerundet ist {gerundet}")

# Absolutwert
print(f"Der Absolutwert von -4 ist {math.fabs(-4)}")

#
# Potenzen und Wurzeln
#
print(f"2^2 = {math.pow(2, 2)}")
print(f"2^3 = {math.pow(2, 3)}")

# Quadratwurzel und Kubikwurzel
print(f"Quadratwurzel von 16 = {math.sqrt(16)}")
print(f"Kubikwurzel von 8 = {math.pow(8, 1/3)}")

#
# Mathematische Konstanten
#

# Pi und e
print(f"Pi = {math.pi}")
print(f"e = {math.e}")

# Andere Konstanten wie tau (2*pi) und die Euler-Mascheroni-Konstante
print(f"Tau = {math.tau}")
print(f"Euler-Mascheroni-Konstante = {math.euler_gamma}")

#
# Exponential- und Logarithmusfunktionen
#
print(f"e^3 = {math.exp(3)}")

# Natürlicher Logarithmus
print(f"ln(e^3) = {math.log(math.exp(3))}")

# Logarithmus Basis 10
print(f"log10(1000) = {math.log10(1000)}")


#
# Trigonometrische Funktionen
#
# Winkel in Grad
winkel_grad = 60

# Umwandlung von Grad in Radiant
winkel_rad = math.radians(winkel_grad)

# Berechnung von Sinus, Kosinus und Tangens
sinus = math.sin(winkel_rad)
kosinus = math.cos(winkel_rad)
tangens = math.tan(winkel_rad)

print(f"Sinus von {winkel_grad}°: {sinus}")
print(f"Kosinus von {winkel_grad}°: {kosinus}")
print(f"Tangens von {winkel_grad}°: {tangens}")