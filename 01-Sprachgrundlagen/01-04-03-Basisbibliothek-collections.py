#
# collections ist eine Grundlegende Basisbibliothek die über die grundlegenden eingebauten Typen wie list, dict, set und tuple hinausgehen
#
#from collections import Counter
import collections

# Eine Liste von Elementen
elemente = ['rot', 'blau', 'rot', 'grün', 'blau', 'blau']

# Erstellen eines Counter-Objekts
element_zähler = collections.Counter(elemente)

print(element_zähler)  # Ausgabe: Counter({'blau': 3, 'rot': 2, 'grün': 1})

# Zugriff auf die Anzahl eines spezifischen Elements
print(f"Anzahl von 'blau': {element_zähler['blau']}")

# Häufigste Elemente finden
print(element_zähler.most_common(2))  # Die zwei häufigsten Elemente

#
#  defaultdict - Standardwerte für fehlende Schlüssel
#

# defaultdict mit einer Liste als Standardwert
dd = collections.defaultdict(list)

# Hinzufügen von Elementen zu einem Schlüssel
dd['hunde'].append('Rex')
dd['hunde'].append('Bello')
dd['katzen'].append('Mauzi')

print(dd)  # Ausgabe: defaultdict(<class 'list'>, {'hunde': ['Rex', 'Bello'], 'katzen': ['Mauzi']})

#
# OrderedDict - Erhalt der Einfügereihenfolge
#
# Erstellen eines OrderedDict-Objekts
od = collections.OrderedDict()

od['Banane'] = 3
od['Apfel'] = 4
od['Birne'] = 1

# Elemente in der Reihenfolge ihres Hinzufügens ausgeben
for key, value in od.items():
    print(key, value)

#
# Doppelseitige Warteschlange
#
# Erstellen eines deque-Objekts
d = collections.deque()

# Hinzufügen von Elementen am Ende
d.append('1')
d.append('2')

# Hinzufügen von Elementen am Anfang
d.appendleft('0')

print(d)  # Ausgabe: deque(['0', '1', '2'])

# Entfernen und Zurückgeben des rechten Elements
rechtes_element = d.pop()
print(f"Entferntes rechtes Element: {rechtes_element}")  # Ausgabe: 2

# Entfernen und Zurückgeben des linken Elements
linkes_element = d.popleft()
print(f"Entferntes linkes Element: {linkes_element}")  # Ausgabe: 0    


    