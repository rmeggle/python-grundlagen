zahlenListe = [1, 2, 4, 5]
print (zahlenListe) # [1, 2, 4, 5]

zahlenListe.append(6)
print (zahlenListe) # [1, 2, 4, 5, 6]

zahlenListe.insert(2,3)
print (zahlenListe) # [1, 2, 3, 4, 5, 6]

zahlenListe.remove(3)
print (zahlenListe) # [1, 2, 4, 5, 6]

zahlenListe.pop(3)
print (zahlenListe) # [1, 2, 4, 6]

zahlenListe.reverse()
print (zahlenListe) # [6, 4, 2, 1]

zahlenListe.sort()
print (zahlenListe) # [1, 2, 4, 6]

# Artikel, Bezeichnung, Preis
# B12;Blauschimmelkäse;12,25
zeile = "B12;Blauschimmelkäse;12,25"
zeilenListe = zeile.split(";")
print (zeilenListe) # ['B12', 'Blauschimmelkäse', '12,25']

preisElement = zeilenListe[-1]                  # Problematisches Element isolioeren
preisElement = preisElement.replace(",",".")    # Element korrigieren (Komma zu Punkt)
preis = (float(preisElement))                   # Element in den richtigen Typ konvertieren
zeilenListe[2] =preis                           # Element ersetzen

print (zeilenListe)

ausgabe=["_"] * 15
print (ausgabe)             # ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_']
print(' '.join(ausgabe))    # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
