Dict1 = {1: 'Python', 2: 'C#', 3: 'Java'}
print(Dict1) # {1: 'Python', 2: 'C#', 3: 'Java'}

print (Dict1[1])        # Python

Dict2 = {"Donald":12, "Dasy":11, "Dagobert":8}
print (Dict2["Dasy"])   # 11

print (Dict2)           # {'Donald': 12, 'Dasy': 11, 'Dagobert': 8}
del Dict2["Dasy"]
print (Dict2)           # {'Donald': 12, 'Dagobert': 8}


name_first = "Donald"
name_last = "Duck"
monthly_salary = 123.45
my_dict = {
            "firstname": name_first,
            "lastname":name_last,
            "salary":monthly_salary
        }
print ("Hello mr. {firstname} {lastname}, you've earned {salary} EUR!".format(**my_dict))
