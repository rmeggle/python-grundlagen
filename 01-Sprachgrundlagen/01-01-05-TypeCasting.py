"""
Unter "typecasting" versteht man das "Umwandeln" von Variablen eines typus zu einem anderen typus. 
Beispiel: Von einem String(var) in eine Ganzzahl (int)

Hierbei bietet python drei Grundfunktionen an: 
foo=int(bar)   : Wandelt die Variable "bar" in eine Ganzzahl von Typ int 
foo=float(bar) : Wandelt eine Variable "bar" in eine Fliesskommazahl von Typ float
foo=str(bar)   : Wandelt eine Variable "bar" in einen String

"""

# Es wird aber nicht gerundet, sondern abgeschnitten:
x = int(1)   # x wird 1
y = int(2.8) # y wird 2
z = int("3") # z wird 3

x = float(1)     # x wird 1.0
y = float(2.8)   # y wird 2.8
z = float("3")   # z wird 3.0
w = float("4.2") # w wird 4.2

x = str("s1") # x wird 's1'
y = str(2)    # y wird '2'
z = str(3.0)  # z wird '3.0' 