#
# Einlesen von der Kommandozeile
#
first_name = input("Bitte Vornamen eingeben:")
var = f"Der eingegebene Name ist:{first_name}"

# hierbei muss auf den richtigen Variablentyp geachtet werden: 

alter = 16

# funktioniert nicht: 
#var = "Der ""Kind"" ist laut DSGVO bis zu einem Alter von " + alter + " Jahren definiert!"
# funktioniert: 
var = "Das \"Kind\" ist laut DSGVO bis zu einem Alter von " + str(alter) + " Jahren definiert!"

# Besser ist es mit sog. "templates" zu arbeiten: 
Text = "Das \"Kind\" {} ist {} Jahre alt!".format(vorname, alter)

#
# Ab Python 3 stehen die f-strings und die %-Formatierungen zur Verfügung:
#

# Zur besseren Lesbarkeit - so können Sie auch die "f-Strings" verwenden:
Text = f"Das \"Kind\" {vorname} ist {alter} Jahre alt!"
Text = "%s ist %i Jahre alt" % (vorname, alter)
