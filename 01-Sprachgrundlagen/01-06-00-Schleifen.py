# Es gibt in Python zwei Arten von Schleifen: for und While

# for-Schleifen:

zahlenListe = [2, 3, 5, 7, 11]
for zahl in zahlenListe:
    print (zahl)

zahlenTupel = (2, 3, 5, 7, 11)
for zahl in zahlenTupel:
    print (zahl) 

zahlenSet = {5 , 3 , 1 , 7 }    
for zahl in zahlenSet:
    print (zahl) 

Dict1 = {1: 'Python', 2: 'C#', 3: 'Java'}
for key,value in Dict1.items():
    print (key,value)

# oder auch range: range(<<start>>, <<ende>> [,<<schrittweite>>])
generierteListe = range(1, 10)
for zahl in generierteListe:
    print(zahl)


# while-Schleife:
z = 0 
while z < 10:
    print (z)
    z=z+1

buchstabe = "a"
wort = "hangman"
if buchstabe in wort:
    for index, w_buchstabe in enumerate(wort):
        if w_buchstabe == buchstabe:
            print("gefunden: Buchstabe {0} an Position {1} im Wort {2}".format(w_buchstabe,index,wort))
