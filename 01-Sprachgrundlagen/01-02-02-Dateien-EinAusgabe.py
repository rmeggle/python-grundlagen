import csv

feld_zum_schreiben = [
    ["Datum","Höchster Wert","Niedrigster Wert","Durschnittlicher Wert","Valide","Nummer","Betrag"],
    ["01.01.2019",510,220,365,True,'','10'],
    ["01.02.2019",610,320,465,False,'30','20'],
    ["03.02.2019",610,'',465,True,'','21']
]  

# Schreiben:
with open('../tmp/example_csv.csv', 'w',encoding='utf-8') as csv_datei:
    writer = csv.writer(csv_datei)
    for zeile in feld_zum_schreiben:
        writer.writerow(zeile)

# Lesen (allgemein):
feld_zum_lesen=[]  
with open('../tmp/example_csv.csv', 'r',encoding='utf-8') as csv_datei:
    reader = csv.reader(csv_datei, delimiter=',')
    for zeile in reader:
        feld_zum_lesen.append(zeile)

for zeile in feld_zum_lesen:
        print(zeile)          

# Lesen (insbesondere bestimmter Spalten der csv):
liste_werte_h=[]
with open('../tmp/example_csv.csv', 'r') as csv_datei:
    # Verwenden der ersten Zeile als Schlüsselnamen
    csv_reader = csv.DictReader(csv_datei)
    for row in csv_reader:
        wert_h = row['Höchster Wert']  # Liest den Wert der Spalte 'Höchster Wert'
        liste_werte_h.append(wert_h)        

for zeile in liste_werte_h:
    print(zeile)       