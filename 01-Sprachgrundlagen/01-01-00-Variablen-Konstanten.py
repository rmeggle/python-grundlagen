# was ist eine Variable? 
# => Duden sagt: variable ist eine veränderliche Größe

##########################################################################################
# Regeln für Benennung für Variablen:
# -----------------------------------
# Für Variablennamen (auch „Bezeichner“ genannt) gelten folgende Regeln:
#   -   Sie müssen entweder mit einem Buchstaben oder mit einem Unterstrich anfangen. 
#       Daran kann sich eine beliebige Abfolge von Buchstaben, Zahlen oder Unterstrichen anschließen.
#   -   Die Buchstaben können Groß- oder Kleinbuchstaben sein. Groß- und Kleinschreibung wird unterschieden; 
#       "a" ist also nicht dasselbe wie "A".
#   -   Es dürfen keine anderen Zeichen als Buchstaben, Zahlen und der Unterstrich benutzt werden. Leerzeichen, Interpunktionszeichen und anderes sind in Variablennamen nicht zulässig:
#       ~ ` ! @ # $ % ^ & * ( ) ; – : " ' < > , . ? / { } [ ] + = /
#   -   Das einzige erlaubte Sonderzeichen ist der Unterstrich.
#   -   Es ist sinnvoll, wenn Variablen ...
#           - immer klein geschrieben werden
#           - immer in Englisch verfasst sind
#           - kein "CamelCase", sondern wenn, dann mit "unter_strich" gekennzeichnet werden (somit können wir leicht erkennen, ob es eine Variable von "uns" ist )

zahl = 1 # jetzt ist der Wert in zahl eine 1
zahl = 2 # jetzt ist der Wert in zahl eine 2
zahl = zahl + 1 # jetzt ist der Wert in zahl eine 3

# python ist "case sensitive": Unterscheidung nach Groß/Kleinbuchstaben:

Sprache = "Python"
sprache = "python"

print (Sprache)
print (sprache)

# Variablenzuweisung ist immer von links nach rechts: 

zahl = 6
zahl = zahl / 2
zahl = zahl * 3

# ist es immer von links nach rechts?
ergebnis = 6/2*3
# => was ist das Ergebnis? 9 oder 1? Richtig: 9

z = 2
z += 2 # entspricht: z=z+2
z -= 2 # entspricht: z=z-2
z *= 2 # entspricht: z=z*2
z /= 2 # entspricht: z=z/2

num_1 = 42
num_2 = 7
num_3 = 6.1
print(num_1 + num_2) # Addition
print(num_1 - num_2) # Subtraktion
print(num_1 * num_2) # Multiplikation
print(num_1 / num_2) # Division
print(num_1 ** num_2) # Exponent
print(num_1 % num_2) # Rest einer ganzzahligen Division (Modulo)
print(num_1 // num_3) # Ganzzahliger Anteil der Integer Division

# Und konstanten? In anderen Programmiersprachen ist das ja möglich?
# Nein - für sich selbst kann man in python keine Konstanten definieren... 
# Ausser mit dem Schlüsselwort "global", aber dazu später
