zahlenSet = set([5 , 3 , 1 , 7 ])
# oder:
zahlenSet = {5 , 3 , 1 , 7 }

print (zahlenSet) # {1, 3, 5, 7}

zahlenSet.add(12)
print (zahlenSet) # {1, 3, 5, 7, 12}

zahlenSet.add(7)
print (zahlenSet) # {1, 3, 5, 7, 12}


Set1 = {1,3,4,2,3}
Set2 = {2,3,4}
print (Set1)        # {1, 2, 3, 4}
print (Set2)        # {2, 3, 4}

Set3 = Set1|Set2    # Union (=>Zusammenführen beider set's)
print (Set3)        # {1, 2, 3, 4}