#Installation von Python

Interpreter: http://www.python.org/download/ 

#Installation einer IDE
(Hinweis: python hat bereits schon eine integriert)  


* **pycharm** https ://www.jetbrains.com/pycharm/download/  
* **Visual Studio Code** : https://code.visualstudio.com/download  

oder online: https://www.programiz.com/

# Kapitel 1:

* 01 Grundlagen der Sprache
* 02 Ein- und Ausgabe 
* 03 Erstellen stabiler Anwendungen mit try/catch
* 04 Übersicht der Basisbibliotheken
* 05 Listen, Dictionaries und Tupel
* 06 Schleifen
* 07 Anlegen virtueller Umgebungen

# Kapitel 2

* 01 Funktionen
* 02 Globale und lokale Variablen
* 03 return vs. yield
* 04 Best practice

# Kapitel 3

* 01 Objektorientierung (basics)
* 02 Vererbung (basics)

# Kapitel 4

* tkinker
* flask (basics)
* eel (basics)
* mathplot (basics)